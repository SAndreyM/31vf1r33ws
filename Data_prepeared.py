# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 06:20:13 2020

@author: Alvl_SAM
"""
import jsonlines
import random

PATH_TRAIN = './MuSeRC/train.jsonl'
PATH_TEST = './MuSeRC/test.jsonl'

train = []
test = []

with jsonlines.open(PATH_TRAIN) as reader:
    for obj in reader:
        train.append(obj)
reader.close()

with jsonlines.open(PATH_TEST) as reader:
    for obj in reader:
        test.append(obj)
reader.close()

train_x = []
test_x = []

buf_str_text = ""
buf_str_q = ""
buf_str_a = ""




for data in train:
    buf_str_text = "<s>Текст: " + data['passage']['text'] + '\n'
    for quest in data['passage']['questions']:
        buf_str_q = "Вопрос: " + quest['question'] + '\n'
        for answer in quest['answers']:
            if answer['label'] == 1:
                buf_str_a = buf_str_text + buf_str_q + "Ответ: " + answer['text'] + '\n' + "Правильность: верно</s>" + '\n'
                train_x.append(buf_str_a)
            if answer['label'] == 0:
                buf_str_a = buf_str_text + buf_str_q + "Ответ: " + answer['text'] + '\n' + "Правильность: ложно</s>" + '\n'
                train_x.append(buf_str_a)




for data in test:
    buf_str_text = "Текст: " + data['passage']['text'] + '\n'
    for quest in data['passage']['questions']:
        buf_str_q = "Вопрос: " + quest['question'] + '\n'
        for answer in quest['answers']:
            buf_str_a = buf_str_text + buf_str_q + "Ответ: " + answer['text'] + '\n' + "Правильность: "
            test_x.append(buf_str_a)


i = 0

data = 0
with open("test.txt", "w", encoding=('utf-8')) as file:
    for data in range(0, 100):  
        file.write(test_x[data])
        print(data)
file.close()
with open("test1.txt", "w", encoding=('utf-8')) as file:
    for data in range(100, 200):  
        file.write(test_x[data])
        print(data)
file.close()
with open("test2.txt", "w", encoding=('utf-8')) as file:
    for data in range(200, 323):  
        file.write(test_x[data])
        print(data)
file.close()






random.shuffle(train_x)

i = 0


with open("train.txt", "w", encoding=('utf-8')) as file:
    for data in train_x:  
        if i <= len(train_x) - 30:
            file.write(data)
        i += 1
file.close()

i = 0
with open("valid.txt", "w", encoding=('utf-8')) as file:
    for data in train_x:  
        if i + len(train_x) - 30  < len(train_x):
            file.write(train_x[i + len(train_x) - 30])
        i += 1
file.close()

